


    <div class="container-fluid background">

        <div class="row nav-row">
            <div class="col-sm-2 nav-icon">
                <div><img src="{{ URL::asset('image/portfolio logo.png') }}" width="68px" height="68px"></div>
            </div>
            <div class="col-sm-2 nav-text">
            <div><strong>JAMES CONNELLY<br>DEVELOPMENTS</strong></div>
            </div>

            <div class="col-sm-8 nav-buttons">
                <ul>
                <li><button type="button" class="btn btn-dark disabled">Home</button></li>
                <li><button type="button" class="btn btn-dark">About</button></li>
                <li><button type="button" class="btn btn-dark">Developer Tools</button></li>
                <li><button type="button" class="btn btn-dark">Contact</button></li>
            </ul>
            </div>
        </div>
        


<!--<div class="container-fluid background">

    <div class="container-fluid navbar">

        <div class="nav-icon">
            <img src="{{ URL::asset('image/portfolio logo.png') }}" width="68px" height="68px">
        </div>

        <div class="nav-info">
             <div class="nav-name"><strong>JAMES CONNELLY</strong></div>    
             <div class="nav-title"><strong>DEVELOPMENTS</strong></div>
        </div> 

    <div class="container-fluid navbar-buttons">   
        <div class="nav-buttons">
            <ul>
                <li><button type="button" class="btn btn-dark disabled">Home</button></li>
                <li><button type="button" class="btn btn-dark">About</button></li>
                <li><button type="button" class="btn btn-dark">Developer Tools</button></li>
                <li><button type="button" class="btn btn-dark">Contact</button></li>
            </ul>
        </div>
    </div> 

    <div class="container heading">
        <div class="heading-title">Junior Front-end Web Developer.</div>
        <div class="heading-desc">
            <p>
            Seeking the next step in career path, learning from truly talented developers.           
            </p>
        </div>
    </div>
</div>-->
<!--@import 'css-layouts/navbar.scss'; include this back in the app.scss file after -->