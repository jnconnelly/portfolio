        
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Aleo" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{ mix('/css/app.css') }}">
        <!-- Favicon (web icon) -->
        <link rel="shortcut icon" href="{{ URL::asset('image/favicon.ico?')}}" type="image/x-icon">
        <link rel="icon" href="{{ URL::asset('image/favicon.ico?')}}" type="image/x-icon">