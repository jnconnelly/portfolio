<!DOCTYPE html>
<html>
<head>
    @include('includes.head-layout')
    <title>JC Developments</title>
</head>
    <body>

        <!-- Nav Bar -->
        <div class="container-fluid">
            <div class="row" id="nav-row1">
                <div class="col-md-12" id="nav-col1">
                     <nav class="navbar navbar-expand-sm navbar-dark bg-dark sticky-top">
                        <a class="navbar-brand"><img src="{{ URL::asset('image/nav-icon.png') }}" width="170px" height="40px"></a>
                        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarsExample04" aria-controls="navbarsExample04" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse justify-content-end" id="navbarsExample04">
                            <ul class="navbar-nav">
                                <li class="nav-item active">
                                    <a class="nav-link">Home <span class="sr-only">(current)</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#about">About</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#developer-tools">Developer Tools</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#contact">Contact</a>
                                </li>
                                </ul>
                        </div>
                    </nav>
                </div> 
            </div>
        </div> 

    <!-- Header -->
    <div class="container-fluid">
            <div class="row" id="header-row1">
                <div class="col-md-12" id="header-col1">
                    <div class="row justify-content-around" id="header-row2">
                        <div class="col-md-10 text-center align-self-center">
                            <h1 id="header-h1">Junior Front-end Web Developer</h1>
                            <hr id="header-hr">
                            <h3 id="header-h3">Taking the next step in career path, learning from truly talented developers.</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <!-- About The Developer -->
    <div class="container-fluid" id="about">
        <div class="row" id="about-row1">
            <div class="col-md-12 text-center" id="about-col1">
                <hr class="main-hr">
                <h1 class="main-h1">About The Developer</h1>
                <hr class="main-hr">
                    <div class="row justify-content-around" id="about-row2">
                        <div class="col-md-10" id="about-col2">
                        <p id="about-p">Since finishing my fixed term contract in September, I decided this was the perfect opportunity to embark on a career change. A career I would thrive from and feel genuinely at home with, this of course being Web Development. Im sure you can agree that Life is immensely short lived meaning opportunities do not come around especially often, I feel timing was gifted to me in order to eventually fullfil my dreams. <br><br>
                        I am now six months deep into web development, I feel extremely passionate along with eager to demonstrate my knowledge of coding, my ambition and my strive to take on any challenge that i am given.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Developer Tools -->
    <div class="container-fluid" id="developer-tools">
        <div class="row" id="devtools-row">
            <div class="col-md-12 text-center" id="devtools-col1">
                <hr class="main-hr">
                <h1 class="main-h1">Developer Tools</h1>
                <hr class="main-hr">
                <!-- HTML + CSS LOGO -->
                <div id="devtools-row" class="row justify-content-around">
                    <div class="col-md-6">
                        <img src="{{ URL::asset('image/html-logo.png') }}" width="100px" height="100px">
                            <div id="icon-description" class="row">
                                <div class="col-md-12"><h4>HTML 5</h4></div>
                            </div>
                    </div>

                    <div class="col-md-6">
                        <img src="{{ URL::asset('image/css3-logo.png') }}" width="70px" height="100px">
                            <div id="icon-description" class="row">
                                <div class="col-md-12"><h4>CSS3</h4></div>
                            </div>
                    </div>
                </div>
                <!-- JS + NODE LOGO -->
                <div id="devtools-row" class="row justify-content-around">
                    <div class="col-md-6">
                        <img src="{{ URL::asset('image/js-logo.png') }}" width="100px" height="100px">
                            <div id="icon-description" class="row">
                                <div class="col-md-12"><h4>Javascript</h4></div>
                            </div>
                    </div>

                    <div class="col-md-6">
                        <img src="{{ URL::asset('image/nodejs-logo.png') }}" width="100px" height="100px">
                            <div id="icon-description" class="row">
                                <div class="col-md-12"><h4>Node JS</h4></div>
                            </div>
                    </div>
                </div>
                <!-- SUBLIME + LARAVEL LOGO -->
                <div id="devtools-row" class="row justify-content-around">
                    <div class="col-md-6">
                        <img src="{{ URL::asset('image/sublime-logo.png') }}" width="100px" height="100px">
                            <div id="icon-description" class="row">
                                <div class="col-md-12"><h4>Sublime Text</h4></div>
                            </div>
                    </div>

                    <div class="col-md-6">
                        <img src="{{ URL::asset('image/laravel-logo.png') }}" width="100px" height="100px">
                            <div id="icon-description" class="row">
                                <div class="col-md-12"><h4>Laravel</h4></div>
                            </div>
                    </div>
                </div>
                <!-- SUBLIME + LARAVEL LOGO -->
                <div id="devtools-row" class="row justify-content-around">
                    <div class="col-md-6">
                        <img src="{{ URL::asset('image/bootstrap-logo.png') }}" width="160px" height="100px">
                            <div id="icon-description" class="row">
                                <div class="col-md-12"><h4>Bootstrap</h4></div>
                            </div>
                    </div>

                    <div class="col-md-6">
                        <img src="{{ URL::asset('image/sass-logo.png') }}" width="100px" height="100px">
                            <div id="icon-description" class="row">
                                <div class="col-md-12"><h4>Sass</h4></div>
                            </div>
                    </div>
                </div>
                <!-- JQUERY +  LOGO -->
                <div id="devtools-row" class="row justify-content-around">
                    <div class="col-md-6">
                        <img src="{{ URL::asset('image/jq-logo.png') }}" width="180px" height="80px">
                            <div id="icon-description" class="row">
                                <div class="col-md-12"><h4>Jquery</h4></div>
                            </div>
                    </div>

                    <div class="col-md-6">
                        <img src="{{ URL::asset('image/git-logo.png') }}" width="100px" height="80px">
                            <div id="icon-description" class="row">
                                <div class="col-md-12"><h4>Git</h4></div>
                            </div>
                    </div>
                </div>
                <!-- COMPOSER + XXAMP LOGO -->
                <div id="devtools-row" class="row justify-content-around">
                    <div class="col-md-6">
                        <img src="{{ URL::asset('image/composer-logo.png') }}" width="100px" height="80px">
                            <div id="icon-description" class="row">
                                <div class="col-md-12"><h4>Composer</h4></div>
                            </div>
                    </div>

                    <div class="col-md-6">
                        <img src="{{ URL::asset('image/xxamp-logo.png') }}" width="100px" height="80px">
                            <div id="icon-description" class="row">
                                <div class="col-md-12"><h4>Xxamp</h4></div>
                            </div>
                    </div>
                </div>
                <!-- PHPMYADMIN + MYSQL LOGO -->
                <div id="devtools-row" class="row justify-content-around">
                    <div class="col-md-6">
                        <img src="{{ URL::asset('image/phpmyadmin-logo.png') }}" width="100px" height="80px">
                            <div id="icon-description" class="row">
                                <div class="col-md-12"><h4>phpmyadmin</h4></div>
                            </div>
                    </div>

                    <div class="col-md-6">
                        <img src="{{ URL::asset('image/mysql-logo.png') }}" width="100px" height="80px">
                            <div id="icon-description" class="row">
                                <div class="col-md-12"><h4>mysql</h4></div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid" id="contact">
            <div class="row" id="contact-row">
                <div class="col-md-12 text-center" id="contact-col1">
                    <hr class="main-hr">
                    <h1 class="main-h1">Contact The Developer</h1>
                    <hr class="main-hr">
                        <div id="contact-row2" class="row justify-content-around">
                            <div id="contact-col2" class="col-md-12">
                                    <form id="form">
                                        @csrf
                                        <div class="row">
                                            <div class="col-6">
                                              <div class="form-group" id="forminput-left">
                                                <label for="firstname-add">First Name</label>
                                                <input type="text" class="form-control" id="firstname-add" placeholder="Enter First Name">
                                              </div>
                                            </div>

                                              <div class="col-6">
                                              <div class="form-group"  id="forminput-left">
                                                <label for="lastname-add">Last Name</label>
                                                <input type="text" class="form-control" id="lastname-add" placeholder="Enter Last Name">
                                              </div>
                                            </div>
                                        </div>  

                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group" id="forminput-center">
                                                <label for="email-add">Email Address</label>
                                                <input type="email" class="form-control" id="email-add" aria-describedby="emailhelp" placeholder="Enter Email">
                                                <small id="emailhelp" class="form-text">We'll never share your email with anyone else.</small>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group" id="forminput-center">
                                                    <label for="query-add">Description Of Query</label>
                                                    <textarea class="form-control" id="query-add" rows="4" placeholder="Text Here"></textarea>
                                                </div>
                                            </div>
                                        </div>

                                    <button type="submit" class="btn btn-primary">
                                    Submit</button>
                                          
                                    </form>
                            </div>
                        </div>
                </div>
            </div>
        </div>

    <div class="container-fluid">
        <div class="row" id="footer-row">
            <div class="col-md-12 text-center" id="footer-col1">
                    <div class="row justify-content-around" id="footer-row2">
                        <div class="col-md-10" id="footer-col2">
                            <h4 id="footer-h4">Company Address:</h4>
                            <p id="footer-p">12 Example Rd<br>Some Village<br>Some City<br>EX11 PLE<br><br>Contact No: 01203405607</p>
                            <hr id="footer-hr">
                            <h4 id="footer-h4">Social Links:</h4>
                            <ul id="footer-ul">
                            <img src="{{ URL::asset('image/facebook.png') }}" width="40px" height="40px">
                            <img src="{{ URL::asset('image/instagram.png') }}" width="40px" height="40px">
                            <img src="{{ URL::asset('image/linkedin.png') }}" width="40px" height="40px">
                            </ul>
                            <p>&copy Copyright 2019 JC Developments All Rights Reserved</p>
                        </div>
                    </div>
            </div>
        </div>
    </div>     

    @include('includes.body-js')    
    </body>
</html>
